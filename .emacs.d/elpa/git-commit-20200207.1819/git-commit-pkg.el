;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20200207.1819" "Edit Git commit messages" '((emacs "25.1") (dash "20180910") (transient "20190812") (with-editor "20181103")) :commit "8de6ecf5c5c840f8a964c3e5bd4d7a1aedf04e10" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
