;;; phi-search-dired-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "phi-search-dired" "phi-search-dired.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from phi-search-dired.el

(autoload 'phi-search-dired "phi-search-dired" "\
Filter files in dired buffer with phi-search interface.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "phi-search-dired" '("phi-search-dired-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; phi-search-dired-autoloads.el ends here
