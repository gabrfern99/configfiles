;;; -*- no-byte-compile: t -*-
(define-package "swiper" "20190627.1944" "Isearch with an overview. Oh, man!" '((emacs "24.1") (ivy "0.11.0")) :commit "7183491619752f21bbca4ee012f3a0dbafaca9cf" :keywords '("matching") :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :url "https://github.com/abo-abo/swiper")
