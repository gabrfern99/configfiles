;;; -*- no-byte-compile: t -*-
(define-package "org-starter" "20200321.520" "A basic configuration framework for org mode" '((emacs "25.1") (dash "2.12") (dash-functional "1.2.0")) :commit "63ebc53299400be1ebf76f76256fbc05ee553246" :authors '(("Akira Komamura" . "akira.komamura@gmail.com")) :maintainer '("Akira Komamura" . "akira.komamura@gmail.com") :url "https://github.com/akirak/org-starter")
