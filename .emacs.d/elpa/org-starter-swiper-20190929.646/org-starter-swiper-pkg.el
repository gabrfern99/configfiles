;;; -*- no-byte-compile: t -*-
(define-package "org-starter-swiper" "20190929.646" "Swiper for org-starter" '((emacs "25.1") (swiper "0.11") (org-starter "0.2.4")) :commit "63ebc53299400be1ebf76f76256fbc05ee553246" :authors '(("Akira Komamura" . "akira.komamura@gmail.com")) :maintainer '("Akira Komamura" . "akira.komamura@gmail.com") :url "https://github.com/akirak/org-starter")
