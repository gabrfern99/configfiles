
;; Initialize package.el
(require 'package)
(add-to-list 'package-archives (cons "melpa" "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

;; Ensure `use-package' is installed.
(unless (package-installed-p 'use-package)
  (progn (package-refresh-contents)
		 (package-install 'use-package)))

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(require 'use-package)

(use-package try
  :ensure t)

;; (use-package magit
;;   :ensure t)

(use-package which-key
  :ensure t
  :config (which-key-mode))

(defalias 'list-buffers 'ibuffer)

(setq indo-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
;(winner-mode 1)
(use-package ace-window
  :ensure t
  :init
  (progn
	(global-set-key [remap other-window] 'ace-window)
	))

(use-package auto-complete
  :ensure t
  :init
  (progn
	(ac-config-default)
	(global-auto-complete-mode t)
	))

(use-package lua-mode
  :ensure t)

(use-package fish-mode
  :ensure t)

(use-package yasnippet
  :ensure t)

(use-package yasnippet-snippets
  :ensure t)

(use-package smartparens
  :ensure t)

(use-package hungry-delete
  :ensure t)

(use-package smart-tabs-mode
  :ensure t)

(use-package auto-highlight-symbol
  :ensure t)
(global-auto-highlight-symbol-mode t)

(use-package anzu
  :ensure t)

(use-package make-it-so
  :ensure t)

(use-package 0xc
  :ensure t)

(setq yas-snippet-dirs
	  '("~/.emacs.d/snippets"				  ;; personal snippets
		"~/.emacs.d/elpa/yasnippet-snippets-20180922.1928/snippets"
		))

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-c d") 'duplicate-line)

;;look into expand-region
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(compilation-window-height 12)
 '(global-whitespace-mode t)
 '(lua-default-application "lua5.3")
 '(lua-indent-level 4)
 '(magit-diff-arguments (quote ("--no-ext-diff" "-M" "-C")))
 '(package-selected-packages
   (quote
	(0xc make-it-so cheatsheet c-eldoc ac-c-headers linum-relative column-enforce-mode arduino-mode smartparens hungry-delete auto-highlight-symbol auto-hightlight-symbol yasnippet-snippets gradle-mode javadoc-lookup fish-mode yasnippet which-key use-package try lua-mode auto-complete ace-window)))
 '(ps-paper-type (quote a4))
 '(tab-always-indent t)
 '(tab-width 4)
 '(whitespace-action nil)
 '(whitespace-space-regexp "\\( +\\)")
 '(whitespace-style
   (quote
	(face trailing lines empty indentation::tab space-before-tab::tab tab-mark))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-function-name-face ((t (:foreground "yellow"))))
 '(whitespace-indentation ((t (:foreground "firebrick"))))
 '(whitespace-space-after-tab ((t (:background "yellow" :foreground "firebrick")))))
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;(global-column-enforce-mode) ;void??
(column-number-mode)
(global-auto-revert-mode)
(yas-global-mode 1)
(setq-default indent-tabs-mode t)
(global-whitespace-mode t)
(global-hungry-delete-mode t)
(smart-tabs-insinuate 'c 'lua)
(linum-relative-global-mode t)
(global-anzu-mode +1)
(setq confirm-kill-emacs 'y-or-n-p)

;;Java environment
(define-key java-mode-map (kbd "C-c C-c") 'compile)
(setq compile-command "make -k ")
(setq compile-read-command 1)

(use-package javadoc-lookup
  :ensure t)
(javadoc-add-roots "/usr/share/doc/openjdk-8-jdk/api")

(use-package gradle-mode
  :ensure t)
(add-hook 'java-mode-hook '(lambda() (gradle-mode 1)))

(add-hook 'c-mode-hook (lambda ()
						 (setq c-basic-offset 4)))

(use-package column-enforce-mode
  :ensure t)

(use-package linum-relative
  :ensure t)

(defun build-and-run ()
  (interactive)
  (gradle-run "--quiet build run"))
(define-key gradle-mode-map (kbd "C-c C-r") 'build-and-run)

(defun javadoc-build ()
  (interactive)
  (gradle-run "--quiet javadoc"))
(define-key gradle-mode-map (kbd "C-c C-g d") 'javadoc-build)

;;C environment
(c-set-offset 'substatement-open 0)

(use-package ac-c-headers
  :ensure t)

(use-package c-eldoc
  :ensure t)

(setq scroll-preserve-screen-position t)

(add-hook 'c-mode-hook (lambda ()
						 (setq c-basic-offset 4)
						 (setq c-default-style "linux")
						 ))

(add-hook 'c-mode-hook 'c-turn-on-eldoc-mode)

(add-hook 'c-mode-hook
		  (lambda ()
			(add-to-list 'ac-sources 'ac-source-c-headers)
			(add-to-list 'ac-sources 'ac-source-c-header-symbols t)))

;;Lua environment

; Lua MTA:SA api keyword highlighting
; Variables predefined by:MTA SA are highlighted
(font-lock-add-keywords 'lua-mode
	'(("\\<\\(FIXME\\):" 1 'font-lock-warning-face prepend)
	  ("\\<\\(TODO\\):" 1 'font-lock-warning-face prepend)
;sourceTimer -- current timer in callback function.
	  ("\\<\\(sourceTimer\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;iprint -- intelligent print function.
	  ("\\<\\(iprint\\)(.*)" 1 'font-lock-builtin-face prepend)
;tocolor -- convert RGB decimal to RGB hexadeciaml.
	  ("\\<\\(tocolor\\)(.*)" 1 'font-lock-builtin-face prepend)
;sourceResource -- the resource that called the event
	  ("\\<\\(sourceResource\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;sourceResourceRoot -- the root of the resource that called the event
	  ("\\<\\(sourceResourceRoot\\)[, \\.:)\n]"
	   1 'font-lock-builtin-face prepend)
;client -- the client that called the event
	  ("\\<\\(client\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;eventName -- the name of the event ("onResourceStart", "onPlayerWasted" etc.)
	  ("\\<\\(eventName\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;this -- Element, which was attached function-handler.
	  ("\\<\\(this\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;source -- The player or element the event was attached to
	  ("\\<\\(source\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;localPlayer -- returns the player element of the local player.
	  ("\\<\\(localPlayer\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;guiRoot -- returns the root element of all GUI elements.
	  ("\\<\\(guiRoot\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;root -- returns the root element of the server
	  ("\\<\\(root\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;resourceRoot -- returns a resource root element of the resource the
;snippet was executed in
	  ("\\<\\(resourceRoot\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;resource -- returns a resource element of the resource the snippet
;was executed in
	  ("\\<\\(resource\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
;exports -- returns a table of resource names containing all export functions
	  ("\\<\\(exports\\)[, \\.:)\n]" 1 'font-lock-builtin-face prepend)
	  ("\\<\\(localPlayer|client|\\)[, \\.:)\n]\\>" . 'font-lock-builtin-face)
;lua global table names
	  ("^\\(\\w*\\) *= *\n*{\n?\\s-?\\w* ?=? ?\\w*,?[^}]*" 1
	   'font-lock-type-face prepend)
;lua constant table value declaration
	  ("^\\s-+\\([A-Z_]+[A-Z0-9_]*\\) = \\w+,$" 1
	   'font-lock-constant-face prepend)
;lua constant table value use
	  ("[a-zA-Z_]+[a-zA-Z0-9_]*\\.\\([A-Z_]+[A-Z0-9_]*\\)[, )\n]" 1
	   'font-lock-constant-face prepend)
;lua constant class values
	  ("^\\w+\\.\\([[:upper:]_]+\\)" 1 'font-lock-constant-face prepend)))

(define-obsolete-function-alias 'string-to-int 'string-to-number "22.1")

(use-package cheatsheet
  :ensure t)

(cheatsheet-add :group 'Common
				:key "C-x C-c"
				:description "leave Emacs.")

(cheatsheet-add :group 'Common
				:key "C-x C-u"
				:description "uppercase region.")

;;Macros
;;Create java getters and setters
(fset 'getAndSet
   "\C-@\C-@\C-[f\C-f\C-@\C-e\C-b\C-[w\C-[}\C-mpublic \C-y\C-[bget\C-[xupcase-char\C-m\C-e()\C-m{\C-m\C-m\C-p\C-ireturn \C-y\C-[b\C-@\C-a\C-[f\C-f\C-?\C-e;\C-n\C-m\C-mpublic void \C-y\C-[b\C-@\C-a\C-[f\C-[f\C-f\C-?set\C-[x\C-[p\C-m\C-e(\C-y\C-e\C-m{\C-m\C-m\C-p\C-ithis.\C-y\C-[b\C-@\C-a\C-[f\C-f\C-?\C-e = \C-y\C-[b\C-@\C-a\C-[f\C-[f\C-[f\C-[b\C-?\C-e;\C-n\C-m\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-e\C-f")

;;Create java constructor
(fset 'makeConstructor
   "\C-@\C-@\C-[<\C-sclass\C-f\C-[d\C-_\C-[}\C-m\C-y\C-[bpublic \C-e()\C-m{\C-m\C-f\C-m\C-u\C-@\C-u\C-@\C-u\C-@")

;;Add attribute to constructor
(fset 'addToConstructor
   "\C-@\C-@\C-[f\C-f\C-@\C-e\C-b\C-[w\C-[}\C-n\C-e\C-b\C-y, \C-n\C-[}\C-p\C-o\C-i\C-y\C-a\C-@\C-e\C-[b\C-?\C-i\C-e\C-[bthis.\C-e = \C-y\C-[b\C-b\C-@\C-a\C-[f\C-[f\C-[f\C-@\C-@\C-e\C-[b\C-@\C-r=\C-f\C-f\C-?\C-e;\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-n")

;;MTA OOPfy: set origin function to last search and method including
;;double colon to kill ring
(fset 'oopfy
   "\C-s\C-s\C-m\C-@\C-a\C-?\C-d\C-[f\C-y\C-d\C-d(\C-d")

(fset 'umlToClass
   "\C-s--\C-a\C-p\C-[d\C-@\C-@\C-[>\C-mpublic class \C-y\C-m{\C-m\C-m\C-p\C-u\C-@\C-u\C-@\C-@\C-n\C-n\C-?\C-@\C-s--\C-a\C-w\C-@\C-n\C-?\C-@\C-@\C-s{\C-n\C-y\C-u\C-@\C-u\C-@\C-@\C-s/\C-a\C-w\C-sclass\C-[}\C-j\C-y\C-r{\C-n")

(fset 'umlToAttribute
   "\C-d\C-s:\C-m\C-?\C-[\C-?\C-d\C-e \C-y;\C-i\C-a\C-fprivate \C-e\C-f")

(fset 'toStringCreate
   "\C-sclass\C-f\C-@\C-[f\C-[w\C-[>\C-p\C-o\C-m@Override\C-mpublic String toString()\C-m{\C-m\C-m\C-p\C-ireturn \"\C-y{\C-d\C-e\C-m+\"\C-b \C-f}\C-e;\C-u\C-@\C-u\C-@\C-a\C-s{\C-a\C-n")

(fset 'toStringAdd
   "\C-e\C-b\C-@\C-[b\C-[w\C-a\C-@\C-@\C-stoString\C-e\C-s;\C-a\C-o\C-i+ \"\C-y='\C-e + \C-y + \"'\C-e\C-u\C-@\C-u\C-@\C-u\C-@\C-u\C-@\C-n")

; have one dired buffer with the java files, cursor on .java file name
; have another buffer with the Makefile with the cursor on the line to be reused
(fset 'addToMakefile
   "\C-@\C-[f\C-[w\C-xo\C-cd\C-[b\C-[b\C-[d\C-y\C-[y\C-[y\C-xo\C-n")
