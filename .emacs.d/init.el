;; Initialize package.el
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))

(package-initialize)

(defun contextual-menubar (&optional frame)
  "Display the menubar in FRAME (default: selected frame) if on a
    graphical display, but hide it if in terminal."
  (interactive)
:  (set-frame-parameter frame 'menu-bar-lines
					   (if (display-graphic-p frame)
						   1 0)))


(add-hook 'after-make-frame-functions 'contextual-menubar)

(add-to-list 'load-path "~/.emacs.d/lisp/")

(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

(global-set-key "\C-h" 'delete-backward-char)

(require 'awesome-tab)
(awesome-tab-mode t)
(global-set-key (kbd "M-1") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-2") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-3") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-4") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-5") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-6") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-7") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-8") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-9") 'awesome-tab-select-visible-tab)
(global-set-key (kbd "M-0") 'awesome-tab-select-visible-tab)

(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)

;; Ensure `use-package' is installed.
(unless (package-installed-p 'use-package)
  (progn (package-refresh-contents)
		 (package-install 'use-package)))

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(use-package smartparens
:hook (lua-mode c-mode java-mode python-mode LilyPond-mode emacs-lisp-mode)
:config (defalias 'smartparens 'smartparens-mode))

(use-package elpy
  :ensure t
  :init
  (elpy-enable))

;; Enable Flycheck

(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; Enable autopep8

;;(require 'py-autopep8)
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)


(require 'use-package)
(setq use-package-always-ensure t)

(use-package try
  :commands try)
(setq elpy-rpc-python-command "python3")
;; (use-package magit)

(use-package which-key
  :defer 5
  :config (which-key-mode))

(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

(defalias 'list-buffers 'ibuffer)

;; This line solves the CAPSLOCK error in st terminal



(setq indo-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
;(winner-mode 1)
(use-package ace-window
  :bind ([remap other-window] . ace-window))

(use-package auto-complete
  :defer 2
  :config
  (global-auto-complete-mode t)
  (ac-config-default)
  :custom
  (ac-auto-show-menu 1.2)
  (ac-candidate-menu-min 1)
  (ac-delay 1.0))


;(use-package arduino-mode)

(use-package fish-mode
  :commands fish-mode)

(use-package glsl-mode
  :commands glsl-mode)

(use-package deft
  :commands deft
  :custom
  (deft-extensions '("org" "txt"))
  (deft-recursive t))

(use-package yasnippet
  :defer 5
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :defer 5
  :custom
  (yas-snippet-dirs '("~/.emacs.d/snippets")))

(use-package smartparens
  :hook (lua-mode c-mode java-mode LilyPond-mode emacs-lisp-mode)
  :config
  (defalias 'smartparens 'smartparens-mode))

(use-package hungry-delete
  :defer 1
  :config
  (global-hungry-delete-mode t))

(use-package smart-tabs-mode
  :commands smart-tabs-mode)

(use-package multiple-cursors
  :bind (("C-c C-M-c" . mc/edit-lines)
         ("C-c C-a" . mc/mark-all-like-this)
         ("C-c C-n" . mc/mark-next-like-this)
         ("C-c C-p" . mc/mark-previous-like-this)
         ("C-c n" . mc/skip-to-next-like-this)
         ("C-c p" . mc/skip-to-previous-like-this)
         ("C-c S-n" . mc/unmark-next-like-this)
         ("C-c S-p" . mc/unmark-previous-like-this)))


(use-package comment-tags
  :commands comment-tags
  :bind ("C-c t" . comment-tags-list-tags-buffers)
  :hook (c-mode LilyPond-mode)
  :config
  (defalias 'comment-tags 'comment-tags-mode))

(use-package auto-highlight-symbol
  :defer 1
  :config (global-auto-highlight-symbol-mode t)
  :custom (ahs-inhibit-face-list nil "Highlight everything"))

(use-package anzu
  :defer 1
  :config (global-anzu-mode +1))

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-c d") 'duplicate-line)

;;look into multifiles
;;look into expand-region
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-show-menu 1.2 nil nil "Customized with use-package auto-complete")
 '(ac-candidate-menu-min 1 nil nil "Customized with use-package auto-complete")
 '(ac-delay 1.0 nil nil "Customized with use-package auto-complete")
 '(ahs-inhibit-face-list nil nil nil "Highlight everything")
 '(ansi-color-names-vector
   ["#2e3436" "#a40000" "#4e9a06" "#c4a000" "#204a87" "#5c3566" "#729fcf" "#eeeeec"])
 '(c-eldoc-buffer-regenerate-time 600 t nil "Make freezing less frequent")
 '(c-eldoc-cpp-command "/usr/bin/gcc" t nil "Set gcc as the preprocessor")
 '(c-eldoc-cpp-macro-arguments "-E -dD -w -P" t nil "-E: only preprocess")
 '(c-eldoc-includes "`pkg-config sdl2 SDL2_ttf gl glew --cflags` -I./ -I../ " t nil "Customized with use-package c-eldoc")
 '(column-number-mode t)
 '(compilation-window-height 12)
 '(custom-enabled-themes nil)
 '(deft-extensions (quote ("org" "txt")) t nil "Customized with use-package deft")
 '(deft-recursive t t nil "Customized with use-package deft")
 '(display-line-numbers-type (quote relative))
 '(font-use-system-font t)
 '(global-whitespace-mode t)
 '(magit-diff-arguments (quote ("--no-ext-diff" "-M" "-C")))
 '(package-selected-packages
   (quote
	(evil flycheck flymake-perlcritic yalinum counsel-dash ivy-clipmenu magit zeno-theme flymake-python-pyflakes pyenv-mode ranger elpy yasnippet-snippets which-key w3m use-package try smartparens smart-tabs-mode phi-search-mc phi-search-dired make-it-so lua-mode linum-relative javadoc-lookup hungry-delete gradle-mode glsl-mode fish-mode deft comment-tags cheatsheet c-eldoc auto-highlight-symbol anzu ace-window ac-c-headers 0xc)))
 '(ps-paper-type (quote a4))
 '(scroll-bar-mode nil)
 '(tab-always-indent t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(whitespace-action nil)
 '(whitespace-space-regexp "\\( +\\)")
 '(whitespace-style
   (quote
	(face trailing lines empty indentation::tab space-before-tab::tab tab-mark)))
 '(yas-snippet-dirs (quote ("~/.emacs.d/snippets")) nil nil "Customized with use-package yasnippet-snippets"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-function-name-face ((t (:foreground "yellow"))))
 '(whitespace-indentation ((t (:foreground "firebrick"))))
 '(whitespace-space-after-tab ((t (:background "yellow" :foreground "firebrick")))))
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(column-number-mode)
(global-auto-revert-mode)
(setq-default indent-tabs-mode t)
(global-whitespace-mode t)
(global-yalinum-mode t)
(setq confirm-kill-emacs 'y-or-n-p)


;;Java environment
(add-hook 'java-mode-hook
          '(lambda() (define-key java-mode-map (kbd "C-c C-c") 'compile)))
(setq compile-command "make -k ")
(setq compile-read-command 1)

;;C environment
;; (define-key c-mode-map (kbd "C-c C-c") 'compile)
;Interpret ANSI color control characters on compilation buffer
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

(c-set-offset 'substatement-open 0)

(use-package ac-c-headers
  :after auto-complete
  :init
  (add-hook 'c-mode-hook
    (lambda ()
      (add-to-list 'ac-sources 'ac-source-c-headers)
      (add-to-list 'ac-sources 'ac-source-c-header-symbols t))))

(use-package c-eldoc
  :hook c-mode c++-mode
  :config
  (defalias 'c-eldoc 'c-turn-on-eldoc-mode)
  :custom
  (c-eldoc-buffer-regenerate-time 600 "Make freezing less frequent")
  (c-eldoc-cpp-command "/usr/bin/gcc" "Set gcc as the preprocessor")
  (c-eldoc-cpp-macro-arguments "-E -dD -w -P" "-E: only preprocess")
  (c-eldoc-includes "`pkg-config sdl2 SDL2_ttf gl glew --cflags` -I./ -I../ "))

(setq scroll-preserve-screen-position t)

(add-hook 'c-mode-hook (lambda ()
						 (setq c-basic-offset 4)
						 (setq c-default-style "linux")
						 ))

(add-hook 'c-mode-hook
		  (lambda ()
			(define-key c-mode-map (kbd "C-c C-c") 'compile)))

(define-obsolete-function-alias 'string-to-int 'string-to-number "22.1")

(use-package cheatsheet
  :commands cheatsheet-show
  :config
  (cheatsheet-add :group 'Common
                  :key "C-x C-c"
                  :description "leave Emacs.")
  (cheatsheet-add :group 'Common
                  :key "C-x C-u"
                  :description "uppercase region."))
