//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"",	"music",	1,	11},
	/* {"",	"crypto",	0,	13}, */
	/* {"",	"torrent",	20,	7}, */
	/* {"",	"news",		0,	6}, */
	{"", "cat /tmp/recordingicon 2>/dev/null",	0,	17},
	{"",	"memory",	10,	14},
	{"",     "nettraf", 1,    18},
	{"",    "nvidiagpu",    1,     2},
	{"",	"cpu",		10,	13},
	{"",    "cpubars", 1,   19},
	{"",    "disk",         60,      9},
	{"",    "mailbox", 10,    3},
	{"",	"pacpackages",	300,	8},
	/* {"",	"weatherv2",	3600,	5}, */
	/* {"",	"mailbox",	180,	12}, */
	{"",	"volume",	1,	10},
	/* {"",	"battery | tr \'\n\' \' \'",	5,	3}, */
	{"",	"clock",	60,	1},
	{"",	"moonphase",	18000,	5},
	{"",    "kbselect",   5,   16},
	{"",	"internet",	5,	4},
	/* {"",	"help-icon",	0,	15}, */
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char *delim = " ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
