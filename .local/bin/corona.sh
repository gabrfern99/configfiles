#!/bin/bash

if [ $(stat -c "%y" ~/.cache/corona | cut -d' ' -f2 |awk -F ":" '{print $1}') != $(date "+%H") ]; then
	curl -s https://corona-stats.online/Brazil > ~/.cache/corona
fi



egrep "Brazil" ~/.cache/corona |
	awk -F ' ' '{print "😷("$9 ")" "💀("$14 ")"}'
